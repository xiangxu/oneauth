FROM python:3.5
MAINTAINER Xiangxu Liu <xxliu@thoughtworks.com>

RUN apt-get update && apt-get install -y netcat-traditional

COPY . /app

WORKDIR /app
COPY ./requirements.txt /app
RUN pip3 install -r requirements.txt

EXPOSE 8000
ENTRYPOINT ["script/entrypoint"]
CMD ["script/run.sh"]
