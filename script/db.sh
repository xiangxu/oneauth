#! /bin/bash

cmd=$1
POSTGRES_USER=${2:-"one_auth_dev"}
POSTGRES_SERVER=${3:-"localhost"}

if [[ $cmd = "setup" ]]; then
    python3 migrations/manage.py version_control postgresql://taskuser:'!123QazxswEdc'@oneauth:5432/oneauth migrations
    exit $?
fi

if [[ $cmd = "migrate" ]]; then
    python3 migrations/manage.py upgrade --url postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_SERVER:5432/oneauth --repository migrations
    exit $?
fi
